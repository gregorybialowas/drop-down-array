<?php
/* ***
drop down menu function gregbialowas.com / gbstrony.com
You are free to use it without limits. If you put my name or name and URL somewhere in your code, I'd appreciate that.

dropMenu('us_state', '', $states_arr, 'Select State', '', '', '');
***  */

function dropMenu($f_name, $f_value, $f_array, $f_default='', $f_css='', $f_tabindex='', $f_js='')
{
    echo '<select name="' . $f_name . '" id="' . $f_name . '"';

    if ($f_css != '') echo ' class="' . $f_css . '"';
    if ($f_js != '') echo ' ' . $f_js;
    if ($f_tabindex != '') echo ' tabindex="' . $f_tabindex . '"';

    echo ">";

    if($f_default != '') echo "\n<option value=\"\">" . $f_default . "</option>\n";

        while(list($key, $val) = each($f_array))
        {
        $output = "<option value=\"" . $key . "\"";
            if ($key == $f_value)
            {
            $output .= " selected";
            }
        $output .= ">" . $val . "</option>\n";
        echo $output;
        }

    echo"</select>";
    /*  ***      dropMenu function ends      ***  */
}