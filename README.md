	With this function, you will need an array defined somewhere. 
	Maybe in an included file or somewhere within the page itself. 

Let's use few of US States' as an example.

	<?php
		/*  ***    create an array with the States    ***  */
		$states_arr = array (
			'CA' => 'California',
			'NV' => 'Nevada',
			'TX' => 'Texas'
		);

		/*  ***    initialize drop-down function    ***  */
		dropMenu('us_state', '', $states_arr, 'Select State', '', '', '');

		/*  ***    or, since you don't need those extra parameters for now, you can shorten the init code:    ***  */
		dropMenu('us_state', '', $states_arr, 'Select State');
	?>


#How does it look? Here we go:
HTML output:

	<select name="us_state" id="us_state">
		<option value="">Select State</option>
		<option value="CA">California</option>
		<option value="NV">Nevada</option>
		<option value="TX">Texas</option>
	</select>

So this the base, something you call when an User fills the form for the first time.

##PRE-SELECTING output.

Now, most likely you will also need the same User to update his/her information. While at it: sooner or later this will come in handy to already print out the initial selection with the variable taken out from the array.

The second option in the function will help you out here.
All you need to do is to define the initial state.

####Example for California:

	<?php
		dropMenu('us_state', 'CA', $states_arr, 'Select State', '', '', '');
	?>

HTML output:

	<select name="us_state" id="us_state">
		<option value="">Select State</option>
		<option value="CA" selected>California</option>
		<option value="NV">Nevada</option>
		<option value="TX">Texas</option>
	</select>

####Or Nevada:

	<?php
		dropMenu('us_state', 'NV', $states_arr, 'Select Nevada State', '', '', '');
	?>

HTML output:

	<select name="us_state" id="us_state">
		<option value="">Select Nevada State</option>
		<option value="CA">California</option>
		<option value="NV" selected>Nevada</option>
		<option value="TX">Texas</option>
	</select>

The second value in the funtion should take its value dynamically, meaning a simple IF condition will do the job just fine:

		IF
			value is defined
			$my_value = defined value
		ELSE
			$my_value = '';
	
###Final version:

	<?php
		dropMenu('us_state', $my_value, $states_arr, 'Select State', '', '', '');
	?>

...and you're all set.

##Additional parameters.

By default the additional parameters are empty, so all you need to do is to fill the proper variable with your own input.

1 - Add CSS to the dropdown

	<?php
		dropMenu('us_state', 'CA', $states_arr, 'Select State', 'free_db_class', '', '');
	?>

2 - Tabindex.

If you want your form's elements to be focused in particular order, you can add a value to the next-to-last parameter of the dropdown function.

	<?php
		dropMenu('us_state', 'TX', $states_arr, '-Select State-', '', '3', '');
	?>

3 - Inline JavaScript (jump menu)

	<?php
		dropMenu('us_states', '', $states_arr_up, ' - Drop with Jump option - ', '', '', 'onchange="document.location.href=this.value"');
	?>

####Add all three at once

	<?php
		dropMenu('us_states', '', $states_arr_up, 'Drop with all parameters enabled', 'free_db_class', '2', 'onchange="document.location.href=this.value"');
	?>

##[ For more info go here ](http://gregbialowas.com/drop-down-array)